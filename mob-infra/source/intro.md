(intro)=
# Einleitung

Dieser Beitrag beschreibt die Vision,
wie die [Mobilitäts-Wende](mob-turn) durch
[Mobilitäts-Infrastruktur](mob-infra) gelingt und ist noch nicht
(oder vielleicht auch nie) fertig (*WIP: Work in Progress*).

(mob-turn)=
## Mobilitäts-Wende

Unter dem Begriff **Mobilitäts-Wende**
verstehen wir einen Prozess hin zu einer Mobilität,
welche im Einklang mit dem
[Gemeinwohl](https://ecogood.org/)
und dem Lebensraum der Menschheit steht.
Vorraussetzung für die Mobilitäts-Wende ist die
[Mobilitäts-Infrastruktur](mob-infra).

(mdv)=
## Mobilitätsdatenverordnung(MDV)

Mit der **Mobilitätsdatenverordnung (MDV)**
vom 1. Juli 2022 hat der Prozess begonnen,
Daten im Zusammenhang mit der Beförderung von Personen im Gelegenheitsverkehr
(kurz *Taxi*) und Linienverkehr
(kurz ÖPNV)
der Öffenlichkeit zur Verfügung zu stellen.
Unter diese Verordnung fallen alle bereits existierende Daten.
In Deutschland wird die MDV durch den [NAP](nap) umgesetzt.

(nap)=
## Nationaler Zugangspunkt (NAP)

Der Nationale Zugangspunkt
(auch *NAP: National Access Point*)
ist die Umsetzung der
[**Mobilitätsdatenverordnung (MDV)**](mdv).
Die MDV wird durch die Webseite
[Mobilithek](https://mobilithek.info/) implementiert.

Zum aktullen Zeitpunkt (Juni 2023)
ist die Veröffentlichung aller bereits exisitierenden Daten auf der
[Mobilithek](https://mobilithek.info/) noch nicht abgeschlossen.
Ein Teil der statischen
(auch *Soll-Fahrplan*) und ein Großteil der dynamischen
(auch *Echtzeit*) Daten stehen uns noch nicht per NAP zur Verfügung.


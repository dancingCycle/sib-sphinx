(value-system)=
# Wertesystem

Diese Vision basiert auf einem grundlegenden Wertesystem,
welches als gemeinsame Anschauung gilt und vom dem alle Aspekte abgeleitet sind.
Dieses Wertesystem ist wie folgt definiert.

Die Mobilitäts-Wende ist ein Prozess,
welcher kontinuierlichen Veränderungen unterliegt.

Eine digitale Infrastruktur ermöglicht die Mobilitäts-Wende,
wenn sie **dezentral**, **diskriminierungsfrei**, **offen** und **souverän** umgesetzt wird.

(decentralized)=
## Dezentral

Eine digitale Infrastruktur ist dezentral organisiert,
wenn mehrere eigenständige Instanzen unabhängig existieren oder sie föderiert interagieren.
Die Interaktion von dezentralen Entitäten ermögleicht Synergie.
Das Risiko von einem Flaschenhals oder der Abhängigkeit von einer einzigen zentralen Instanz wird auf diese Weise umgangen.

(free-from-discrimination)=
## Diskriminierungsfrei

Alle Menschen sind gleich und in ihrer Mobilitäts-Motivation gleich zu behandeln.

Eine digitale Infrastruktur ist **nicht** diskriminierungsfrei,
wenn die Rechte auf einen Kreis privilegierter Menschen reduziert wird.

(open)=
## Offen

Eine digitale Infrastruktur ist offen gestaltet,
wenn Menschen einen
[diskriminierungsfreien](free-from-discrimination) Zugang erhalten.
Außerdem haben sie das Recht die dafür erforderlichen Informationen,
Software oder Technologie zu
[verwenden, verstehen, verbreiten oder verbessern](https://fsfe.org/freesoftware/freesoftware.de.html).

Eine digitale Infrastruktur ist **nicht** offen,
wenn Informationen, Software oder Technologie der Öffentlichkeit vorenthalten wird.

(sovereign)=
## Souverän

Eine digitale Infrastruktur ist souverän,
wenn wir selbstbestimmt diese Infrastuktur planen,
beauftragen, implementieren, betreiben und kontrollieren können.

Eine digitale Infrastruktur ist **nicht** sourverän,
wenn sie uns als Plattform einen Dienst anbietet
(*PaaS: Platform as a Service*),
an dem sich unser Recht auf die reine Verwendung beschränkt.
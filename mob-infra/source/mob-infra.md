(mob-infra)=
# Mobilitäts-Infrastruktur

Unter dem Begriff **Mobilitäts-Infrastruktur**,
verstehen wir digitale Infrastruktur angefagen bei Daten über Schnittstellen, Software bis hin zu Hardware.

Die Mobilitäts-Infrastruktur ermöglicht die
[Mobilitäts-Wende](mob-turn).

(data-pool)=
## Datenbasis

Qualität und Quantität der Mobilitäts-Infrastruktur sind eng mit der Datenbasis verbunden.
Ohne Daten kann eine Infrastruktur uns keine Dienste anbieten.
Das gilt auch für die Mobilitäts-Infrastruktur.
Dienste wie bspw. dynamische Abfahrtstafel, Anschluss-Sicherung, Echtzeit-Karte oder Reiseplaner basieren auf verschiedene Daten, welche sich in Art, Format und Quelle stark unterscheiden.

(data-type)=
### Datenarten

Im Bereich der Mobilitäts-Infrastruktur unterscheiden wir folgende Datenarten.
* **Adress-Daten**: Fahrgäste sind es gewohnt bei der Reiseplanung Adressen als Start und Ziel anzugehen. Geeignete Dienste erlauben es uns diese Adressen in geografische Koordinaten von bspw. Haltestellen zu übersetzen.
* **Dynamische Verkehrs-Daten**: Unter dynamischen Verkehrs-Daten verstehen wir die aktuelle Position von Verkehrsmitteln, Prognosen zu Abweichungen zum Fahrplan oder Ereignisse mit Einfluss auf den Fahrplan.
* **Geografische Koordinaten**: Um bei der Reiseplanung eine Verbindung zu berechnen sind geografische Koordinaten von Start und Ziel erforderlich.
* **Statische Fahrplan-Daten**: Statische Fahrplan-Daten, auch bekannt als Soll-Fahrplan-Daten, können den Jahres-Fahrplan oder einen Auszug daraus von Verkehrsunternehmen enthalten. Die deutschlandweiten Soll-Fahrplan-Daten im [GTFS](https://mobilithek.info/offers/-1284326438210480997)- oder [NeTEx](https://mobilithek.info/offers/-2380660373077588812)-Format sind per [NAP](nap) erreichbar.
* **Haltestellen-Daten**: Diese Daten beschreiben allgemeine Eigenschaften von Haltestellen wie bspw. Betreiber, Name oder geografische Koordinaten. Die deutschlandweiten [Haltestellen-Daten](https://mobilithek.info/offers/-3126304694992251026) sind per [NAP](nap) erreichbar.
* **Karten-Daten**:  Für die Wege-Wahl (auch Routing) zwischen Start und Ziel sind Karten-Daten erforderlich. Damit ein Fahrgast sich ein Bild machen kann, können wir diese Route oder die aktuelle Position von Verkehrsmitteln auf einer Karte darstellen. Die [OpenStreetMaps-Datenbank](https://www.openstreetmap.org) ist eine vielseitige Quelle weit über Karten-Daten hinaus.

(data-format)=
### Datenformate

Im Bereich der Mobilitäts-Infrastruktur unterscheiden wir folgende Datenformate.

* **[GTFS](https://gtfs.org/schedule/)**
* **[GTFS Realtime](https://gtfs.org/realtime/)**
* **[NeTEx](https://netex-cen.eu/)**
* **[CEN SIRI](https://www.siri-cen.eu/)**
* **[VDV](https://www.vdv.de/downloads.aspx)**

(data-source)=
### Datanquellen

Bei den Datenquellen sind folgende Aspekte von zentraler Bedeutung.

* **Automatische Verarbeitung**: Für die Mobilitäts-Infrastuktur ist die Verfügbarkeit von Daten genauso wichtig wie der automatische Verarbeitung.
* **Lizenz**: Die Datenlizenz ermöglicht uns die Daten frei von [Diskriminierung](free-from-discrimination) und [offen](open) zu verwenden.
* **Daseinsvorsorge**: Daten vom öffentlichen Verkehr betrachten wir als Leistung der Daseinsvorsorge, weil sie von Allgemeininteresse bzw. Allgemeinwohl sind. Durch die [MDV](mdv) haben wir ein Recht auf diese Daten. Aktuell fehlt noch ein entsprechendes Gesetz, um dieses Recht durchzusetzen.

Das Projekt [OpenDataLand](https://opendataland.de/) hat eine umfangreiche [Liste](https://opendataland.de/mapping/) von Datenquellen aus Deutschland erarbeitet.

Das [Deutschen Zentrale für Tourismus e.V. (DZT)](https://www.germany.travel)
pflegt in einem eigenen [Portal](https://open-data-germany.org/)
Daten für Deutschland aus der Perspektive vom Touristmus-Sektor.

(inter-modal-infra)=
## Inter-Modale Infrastruktur

Zur Zeit entwicklet sich die
[inter-modale](https://de.wikipedia.org/wiki/Intermodaler_Verkehr)
Auskunft zur [sozialen Norm](https://de.wikipedia.org/wiki/Soziale_Norm)
bei Fahrgästen.
Mit anderen Worten,
durch die [Mobilitäts-Wende](mob-turn)
und den aktuellen [Zeitgeist](https://de.wikipedia.org/wiki/Zeitgeist)
erwarten Fahrgäste bei der Verbindungs-Suche eine Auswahl verfügbarer
[Verkehrsmittel](https://de.wikipedia.org/wiki/Verkehrsmittel).
Apps von [GAFAM](https://de.wikipedia.org/wiki/Big_Tech)
oder einzelne regionale Auskunfts-Systeme wie der
[Fahrplaner](https://fahrplaner.de)
oder [HVV Switch](https://www.hvv-switch.de)
erfüllen bzw. treiben diese Erwartungen heute bereits.
Neben diesen Einzelfällen verfehlen die meisten Auskunfts-Systeme diese Erwartungen.
Daraus ergibt sich die folgende Fragestellung.

* Wie schaffen wir es,
dass Fahrgäste eine inter-modale Auskunft erhalten,
unabhängig von dem gewählten Auskunfts-System?

Eine inter-modale Auskunft erfordert inter-modale Mobilitäts-Infrastruktur.
Jedoch ist Mobilitäts-Infrastruktur,
in der Regel, als [monolithische](https://de.wikipedia.org/wiki/Monolith)
[Blackbox](https://de.wikipedia.org/wiki/Black_Box_(Systemtheorie))
ohne geeignete [Schnittstellen](https://de.wikipedia.org/wiki/Schnittstelle)
gestaltet.

Aus diesem Grund erhalten wir eine Auskunft über zum Beispiel

* [Carsharing](https://www.dein-carsharing.de/),
* [Fernverkehr in der EU](https://trainsforeurope.eu/),
* [Lasten-Rad](https://cargorocket.de/),
* [Mitfahren](https://www.matchrider.de/),
* [Nachtzüge](https://www.midnight-trains.com),
* [Rad-Verkehrs-Infrastruktur](https://www.aktivmobil-bw.de/radverkehr/raddaten/radvis-bw/) oder
* [Shared Mobility](https://map.geo.admin.ch/?lang=de&topic=energie&bgLayer=ch.swisstopo.pixelkarte-grau&zoom=0&catalogNodes=2419,2420,2427,2480,2429,2431,2434,2436,2767,2441,3206&layers=ch.bfe.shared-mobility)

immer nur einzeln, isoliert und nicht automatisch verwertbar durch Mobilitäts-Infrastruktur.

Ein Lösungsweg ist die [Mobilitäts-Infrastruktur](mob-infra)

* [dezentral](value-system),
* modular und
* interaktiv

zu gestalten.
Mit anderen Worten,
jede in sich abgeschlossene Funktion wird als Modul realisiert und bietet einen Dienst per Standard-Schnittstelle.
Module bieten uns Souveränität bei der Entscheidung,
ob wir selbst zum Betreiber werden oder eine Partnerschaft eingehen.
Sie bieten uns Synergie,
wenn sie [offen](open) gestaltet ist.
Auf dieser Basis kann unser Auskunfts-System mit den
[offen](open) verfügbaren Modulen der
[Mobilitäts-Infrastruktur](mob-infra) interagieren,
welche die Dynamik unserer Region fordert.

(regional-pis)=
## Regionale Auskunft

Eine nationale Auskunft wie der
[DB Navigator](https://www.bahn.de/service/mobile/db-navigator)
ist toll,
weil sie uns eine Übersicht über das Moblitäts-Angebot auf nationaler Ebene liefert.
Sie ist gleichzeitig aber auch **nicht** toll,
weil sie die Dynamik der Region nicht widerspiegelt.

Um das Beste von beiden Welten zu bekommen,
sollten wir beide Welten und gleichzeitig auch die Schnittstellen zwischen beiden Welten unterstützen.
Beide Welten sollen zum Zweck von Auskunft,
Vertrieb, Buchung usw. interagieren können.

Das Tool [Stadtnavi](https://stadtnavi.de/)
ist ein Lösungsweg,
um eine Regionale Auskunft umzusetzen.
Es handelt sich hierbei um einen Baukasten,
welchen wir an die Dynamik der Region anpassen können.
Diese Eigenschaft hat bisher
(Stand April 2023) die folgenden 8 Instanzen hervorgebracht.

* [Stadtnavi Herrenberg](https://herrenberg.stadtnavi.de/)
* [Stadtnavi Angermünde](https://angermuende.bbnavi.de/)
* [Bad Belzig](https://bad-belzig.bbnavi.de/)
* [Fürstenberg Havel](https://fuerstenberg-havel.bbnavi.de/)
* [Herzberg Elster](https://herzberg-elster.bbnavi.de/)
* [Eberswalde](https://eberswalde.bbnavi.de/)
* [Michendorf](https://michendorf.bbnavi.de/)
* [Frankfurt Oder](https://frankfurt-oder.bbnavi.de/)

Die Replizierung der Stadtnavi-Instanzen wird durch die modulare Struktur
(Baukasten) und durch die
[Free Libre Open Source Software (FLOSS)](https://www.swingbe.de/softwarefreedom/)- und
[Sharing-Economie](https://de.wikipedia.org/wiki/Sharing_Economy)
-Philosophie ermöglicht.

(design-mob-service)=
## Planung vom Mobilitäts-Angebot

Von Mobilitäts-Anbieter bis Mobilitäts-Planer,
für alle ist es eine Herausforderung das aktuelle Moblitäts-Angebot zu beurteilen.
Ohne Zweifel:
Die Erwartungen an die
[Mobilitäts-Wende](mob-turn) sind hoch.
Aber,
wie können wir diesen Erwartungen nachgehen,
wenn wir die Lücke zum aktullen Status nicht kennen.

Ein Lösungsweg ist der Einsatz von bestehenen Tools.

Das Tool
[15-Minuten-Stadt](https://15-minuten-stadt.de/)
hilft bei der Planung vom Mobilität-Angebot.
Ziel ist es,
dass alle Wege vom Alltag in weniger als 15 Minuten mit nachhaltigen Verkehsmitteln bestritten werden können.


(intro)=
# Einleitung

Der Verein [DELFI e.V.](https://delfi.de)
stellt ein Produkt mit dem Namen
[DELFI-Datensatz](https://www.delfi.de/de/leistungen-produkte/daten-dienste/)
über die Plattform
[OpenData ÖPNV](https://opendata-oepnv.de) zur Verfügung.
Dieser Datensatz enthält den ÖPNV,
Schienen-Fernverkehr und Fernbusse für Deutschland.

Außerdem stellt der Verein ein Produkt mit dem Namen
[Zentrales Haltestellenverzeichnis (ZHV)](https://www.delfi.de/de/leistungen-produkte/daten-dienste/)
über die Plattform
[zhv](https://zhv.wvigmbh.de) zur Verfügung.

Laut DELFI nutzen Haltestellen im ZHV die
[**DHID: Deutschlandweit einheitliche Haltestellen-ID**](https://de.wikipedia.org/wiki/Haltestelle)
als einen in Deutschland eindeutigen Haltestellen-Bezeichner.
Die DHID ist in dem Dokument
[VDV 432](https://knowhow.vdv.de/documents/432/)
definiert und auch als **Global ID** bekannt.
Bei der Spezifikation **VDV 432** hat sich der
[VDV](https://vdv.de) an dem internationalen 
[IFOPT](https://en.wikipedia.org/wiki/Identification_of_Fixed_Objects_in_Public_Transport)
[CEN](https://de.wikipedia.org/wiki/Europ%C3%A4isches_Komitee_f%C3%BCr_Normung)
-Standard orientiert.

Laut DELFI verwendet auch der DELFI-Datensatz die DHID aus dem ZHV als eindeutigen Haltestellen-Bezeichner.
Für die Umsetzung von diesem Ziel ist DELFI von den Daten-Lieferanten abhängig.
Sie sind es, die Daten an die
[**DELFI-Integrationsplattform (DIP)**](https://www.delfi.de/de/strategie-technik/architektur/)
liefern.
Nur wenn Daten-Lieferanten DHIDs aus dem ZHV als eindeutigen Haltestellen-Bezeichner verwenden,
wird dieser von DELFI in die DIP integriert und pe DELFI-Datensatz exportiert.

Die Schnittstelle
[delfi.api.swingbe.de](https://v1delfi.api.swingbe.de/stops-not-dhid?oset=1&limit=1)
bietet uns Haltestellen aus dem DELFI-Datensatz an,
welche bisher einen Haltestellen-Bezeichner **nicht** konform zur DHID verwenden.

Die Webseite [delfi.swingbe.de](https://delfi.swingbe.de)
zeigt uns die Haltestellen dieser Schnittstelle auf einer Karte und Tabelle an.
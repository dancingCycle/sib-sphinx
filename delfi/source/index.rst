.. getting-started documentation master file, created by
   sphinx-quickstart on Thu Apr 13 17:31:40 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

(WIP) DELFI: Durchgängige ELektronische FahrgastInformation
===========================================================

.. toctree::
   :numbered:
   :maxdepth: 3
   :caption: Contents:

   intro

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
